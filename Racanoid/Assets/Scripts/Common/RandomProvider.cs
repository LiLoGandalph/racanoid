﻿using System;
using System.Security.Cryptography;

public static class RandomProvider
{
    #region Generator
    private static readonly RNGCryptoServiceProvider generator = new RNGCryptoServiceProvider();
    #endregion Generator


    #region Double

    public static double Double()
    {
        byte[] randomNumber = new byte[1];
        generator.GetBytes(randomNumber);

        double randomDouble0To255 = Convert.ToDouble(randomNumber[0]);

        return randomDouble0To255 / 255d;
    }


    public static double Double(double minimum, double maximum)
    {
        double multiplier = Double();
        double range = maximum - minimum;

        return minimum + multiplier * range;
    }

    #endregion Double


    #region Int

    public static int Int(int minimum, int maximum)
    {
        return (int)Math.Round(Double(minimum, maximum));
    }

    #endregion Int


    #region Bool

    public static bool Bool()
    {
        return Int(0, 1) == 0;
    }

    #endregion Bool
}
