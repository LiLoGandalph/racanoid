﻿using UnityEngine;
using System.Runtime.Serialization;
using SerializableTypes;

[System.Serializable]
public struct Theme : ISerializable
{
    public Color mainColor;


    public Theme(Color mainColor)
    {
        this.mainColor = mainColor;
    }



    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        info.AddValue("Main Color", new Color_Ser(mainColor));
    }


    public Theme(SerializationInfo info, StreamingContext context)
    {
        mainColor = (Color_Ser)info.GetValue("Main Color", typeof(Color_Ser));
    }
}



[System.Serializable]
public struct UnlockableTheme
{
    public Theme theme;
    public bool IsUnlocked { get; private set; }


    public UnlockableTheme(Theme theme, bool isUnlocked)
    {
        this.theme = theme;
        this.IsUnlocked = isUnlocked;
    }


    public void Unlock()
    {
        IsUnlocked = true;
    }
}
