﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;



public static class UsefulStuff
{
    public static bool TryFind<C, E>(this C collection, Predicate<E> predicate, out E foundItem)
        where C : IEnumerable<E>
    {
        foreach (var item in collection)
        {
            if (predicate(item))
            {
                foundItem = item;
                return true;
            }
        }

        foundItem = default;
        return false;
    }
}