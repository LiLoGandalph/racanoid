﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Themer : MonoBehaviour
{
    [Serializable]
    public class ThemeableMaterial
    {
        private const string mainColor = "_Color";
        private const string faceColor = "_FaceColor";
        private const string outlineColor = "_OutlineColor";

        public Material material;



        public void ApplyColor(Color color)
        {
            Color tmpColor;

            if (material.HasProperty(mainColor))
            {
                tmpColor = material.GetColor(mainColor);
                color.a = tmpColor.a;
                material.SetColor(mainColor, color);
            }

            if (material.HasProperty(faceColor))
            {
                tmpColor = material.GetColor(faceColor);
                color.a = tmpColor.a;
                material.SetColor(faceColor, color);
            }

            if (material.HasProperty(outlineColor))
            {
                tmpColor = material.GetColor(outlineColor);
                color.a = tmpColor.a;
                material.SetColor(outlineColor, color);
            }
        }
    }


    public Button changeButton;
    public ThemeableMaterial[] mainMaterials;
    public Theme[] themes;

    private int currentTheme;


    private void Awake()
    {
        changeButton.onClick.AddListener(ChangeTheme);
    }


    private void ChangeTheme()
    {
        currentTheme = (currentTheme + 1) % themes.Length;

        foreach (var material in mainMaterials)
        {
            material.ApplyColor(themes[currentTheme].mainColor);
        }
    }
}
