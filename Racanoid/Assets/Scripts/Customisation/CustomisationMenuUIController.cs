﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class CustomisationMenuUIController : MonoBehaviour
{
    [SerializeField]
    private Button backButton;


    private void Awake()
    {
        backButton.onClick.AddListener(LoadMenu);
    }


    private void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
