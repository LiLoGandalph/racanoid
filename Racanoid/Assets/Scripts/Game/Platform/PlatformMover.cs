﻿using System;
using UnityEngine;



public class PlatformMover : MonoBehaviour
{
    [SerializeField]
    private float deltaXMultiplier = 1.5f;

    private new Rigidbody2D rigidbody;

    private float positionOffset = 0f;


    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }


    private void Start()
    {
        InputManager.Instance.touchStart.AddListener(RememberOffset);
        InputManager.Instance.touchMove.AddListener(Move);
    }


    private void RememberOffset(Vector2 touchPosition)
    {
        positionOffset = touchPosition.x - rigidbody.position.x;
    }


    private void Move(Vector2 touchPosition)
    {
        float nextX = touchPosition.x - positionOffset;
        float deltaX = nextX - rigidbody.position.x;
        nextX = rigidbody.position.x + deltaX * deltaXMultiplier;

        Area gameArea = ScreenLayoutController.Instance.ScreenLayout.GameArea;
        nextX = Mathf.Max(gameArea.LeftBound, nextX);
        nextX = Mathf.Min(nextX, gameArea.RightBound);

        var nextPosition = new Vector2(nextX, rigidbody.position.y);

        positionOffset = touchPosition.x - nextX;

        rigidbody.MovePosition(nextPosition);
    }
}