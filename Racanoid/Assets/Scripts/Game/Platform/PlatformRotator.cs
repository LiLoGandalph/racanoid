﻿using SerializableTypes;
using System;
using System.Collections;
using System.Runtime.Serialization;
using UnityEngine;



public class PlatformRotator : MonoBehaviour
{
    [SerializeField]
    private float rotationSpeed;

    [SerializeField]
    private float rotationAngle;


    private const sbyte Forward = 1;


    private new Rigidbody2D rigidbody;
    private sbyte direction = Forward;



    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();

        StartCoroutine(Rotate());
    }


    private IEnumerator Rotate()
    {
        while (true)
        {
            rigidbody.MoveRotation(rigidbody.rotation + direction * rotationSpeed * Time.fixedDeltaTime);

            yield return new WaitForFixedUpdate();

            if (Mathf.Abs(rigidbody.rotation) >= rotationAngle)
            {
                direction *= -1;
            }
        }
    }
}
