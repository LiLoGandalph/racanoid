﻿using SerializableTypes;
using System;
using System.Runtime.Serialization;
using UnityEngine;



public class PlatformController : MonoBehaviour
{
    [SerializeField]
    private PlatformRotator rotator;


    public float PositionY { get; private set; }


    private void Awake()
    {
        AdjustScale();

        float gameAreaBottom = ScreenLayoutController.Instance.ScreenLayout.GameArea.BottomBound;
        float platformWidthHalf = GetComponent<SpriteRenderer>().bounds.extents.x;
        PositionY = gameAreaBottom + platformWidthHalf;

        ResetTransform();
    }

    private void Start()
    {
        GameManager.Instance.resetState.AddListener(ResetTransform);
    }


    public void ResetTransform()
    {
        transform.position = new Vector2(0f, PositionY);
        transform.rotation = new Quaternion(0f, 0f, 0f, 0f);
    }


    private void AdjustScale()
    {
        float updScale = ScreenLayoutController.Instance.UpdatedTileScale;
        transform.localScale = new Vector2(updScale, updScale);
    }
}
