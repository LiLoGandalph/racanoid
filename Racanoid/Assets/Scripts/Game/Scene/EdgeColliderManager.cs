﻿using UnityEngine;
using UnityEngine.Events;



public class EdgeColliderManager : MonoBehaviour
{
    public UnityEvent ballLost;

    public static EdgeColliderManager Instance { get; private set; }

    private Area gameArea;

    private EdgeCollider2D bouncyEdges;
    private EdgeCollider2D deadlyBottom;


    private void Awake()
    {
        Instance = this;

        ballLost = new UnityEvent();

        gameArea = ScreenLayoutController.Instance.ScreenLayout.GameArea;

        bouncyEdges = gameObject.AddComponent<EdgeCollider2D>();
        bouncyEdges.points = new Vector2[]
        {
            new Vector2(gameArea.LeftBound, gameArea.BottomBound),
            new Vector2(gameArea.LeftBound, gameArea.TopBound),
            new Vector2(gameArea.RightBound, gameArea.TopBound),
            new Vector2(gameArea.RightBound, gameArea.BottomBound)
        };

        deadlyBottom = gameObject.AddComponent<EdgeCollider2D>();
        deadlyBottom.points = new Vector2[]
        {
            new Vector2(gameArea.LeftBound, gameArea.BottomBound),
            new Vector2(gameArea.RightBound, gameArea.BottomBound)
        };

        deadlyBottom.isTrigger = true;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<BallController>() != null)
        {
            ballLost.Invoke();
        }
    }
}
