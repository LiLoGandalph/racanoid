﻿using UnityEngine;



[System.Serializable]
public struct AreaNormalised
{
    [SerializeField]
    [Range(-1f, 1f)]
    private float topBound;
    public float TopBound => topBound;

    [SerializeField]
    [Range(-1f, 1f)]
    private float bottomBound;
    public float BottomBound => bottomBound;

    [SerializeField]
    [Range(-1f, 1f)]
    private float leftBound;
    public float LeftBound => leftBound;

    [SerializeField]
    [Range(-1f, 1f)]
    private float rightBound;
    public float RightBound => rightBound;



    public AreaNormalised(float topBound = 1f, float bottomBound = 1f, float leftBound = 1f, float rightBound = 1f)
    {
        this.topBound = topBound;
        this.bottomBound = bottomBound;
        this.leftBound = leftBound;
        this.rightBound = rightBound;
    }


    public override string ToString()
    {
        return $"AreaNormalised {{ top: {topBound}, bottom: {bottomBound}, left: {leftBound}, right: {rightBound} }}";
    }
}