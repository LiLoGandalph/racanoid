﻿using UnityEngine;



[System.Serializable]
public struct Area
{
    [SerializeField]
    private float topBound;
    public float TopBound => topBound;

    [SerializeField]
    private float bottomBound;
    public float BottomBound => bottomBound;

    [SerializeField]
    private float leftBound;
    public float LeftBound => leftBound;

    [SerializeField]
    private float rightBound;
    public float RightBound => rightBound;


    public float Width => RightBound - LeftBound;
    public float Height => TopBound - BottomBound;



    public Area(float topBound, float bottomBound, float leftBound, float rightBound)
    {
        this.topBound = topBound;
        this.bottomBound = bottomBound;

        this.leftBound = leftBound;
        this.rightBound = rightBound;
    }


    public Area(Camera camera)
    {
        float height = camera.orthographicSize;
        float width = height * camera.aspect;

        topBound = height;
        bottomBound = -height;
        leftBound = -width;
        rightBound = width;
    }



    public static Area operator *(Area initialArea, AreaNormalised areaMultiplier)
    {
        return new Area(
            initialArea.TopBound * areaMultiplier.TopBound,
            initialArea.BottomBound * areaMultiplier.BottomBound,
            initialArea.LeftBound * areaMultiplier.LeftBound,
            initialArea.RightBound * areaMultiplier.RightBound);
    }


    public static AreaNormalised operator /(Area smaller, Area bigger)
    {
        return new AreaNormalised(
            smaller.TopBound / bigger.TopBound,
            smaller.BottomBound / bigger.BottomBound,
            smaller.LeftBound / bigger.LeftBound,
            smaller.RightBound / bigger.RightBound);
    }



    public override string ToString()
    {
        return $"Area {{ top: {topBound}, bottom: {bottomBound}, left: {leftBound}, right: {rightBound} }}";
    }
}