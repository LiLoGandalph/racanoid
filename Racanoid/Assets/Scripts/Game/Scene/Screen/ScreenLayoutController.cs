﻿using UnityEngine;



public class ScreenLayoutController : MonoBehaviour
{
    [Header("Layout")]

    [SerializeField]
    private AreaNormalised gameArea;

    [SerializeField]
    [Range(0f, 0.2f)]
    private float tileAreaSideOffset;


    [Header("Grid")]

    [SerializeField]
    private Tile tilePrefab;

    [SerializeField]
    private int gridSize;

    [SerializeField]
    private float tileToPlaceholderRatio;



    public ScreenLayout ScreenLayout { get; private set; }

    public float UpdatedTileScale { get; private set; }

    public Vector2[] SpawnPoints { get; private set; }


    private float tilePlaceholderSize;



    public static ScreenLayoutController Instance { get; private set; }

     

    private void Awake()
    {
        Instance = this;

        ScreenLayout = new ScreenLayout();
        ScreenLayout.Initialise(Camera.main, gameArea, tileAreaSideOffset);



        tilePlaceholderSize = ScreenLayout.TileArea.Width / gridSize;

        SetUpSpawnPoints(ScreenLayout.TileArea);

        AdjustTileScale();
    }



    #region Setup
    private void SetUpSpawnPoints(Area tileArea)
    {
        SpawnPoints = new Vector2[gridSize];

        for (var i = 0; i < gridSize; i++)
        {
            SpawnPoints[i].y = tileArea.TopBound + 0.5f * tilePlaceholderSize;
            SpawnPoints[i].x = tileArea.LeftBound + (0.5f + i) * tilePlaceholderSize;
        }
    }


    private void AdjustTileScale()
    {
        UpdatedTileScale = CalculateNeededTileScale(tilePlaceholderSize);

        tilePrefab.transform.localScale = Vector2.one * UpdatedTileScale;

        TileMover.moveDistance = tilePlaceholderSize;
    }


    private float CalculateNeededTileScale(float tilePlaceHolderSize)
    {
        float currentSize = tilePrefab.GetComponent<SpriteRenderer>().bounds.size.x;
        float currentScale = tilePrefab.gameObject.transform.localScale.x;

        float neededSize = tilePlaceHolderSize * tileToPlaceholderRatio;

        return currentScale / currentSize * neededSize;
    }
    #endregion
}
