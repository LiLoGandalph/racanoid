﻿using UnityEngine;



[System.Serializable]
public class ScreenLayout
{
    [SerializeField]
    private Area screenArea;
    public Area ScreenArea => screenArea;

    [SerializeField]
    private AreaNormalised screenAreaNormalised;
    public AreaNormalised ScreenAreaNormalised => screenAreaNormalised;


    [SerializeField]
    private Area gameArea;
    public Area GameArea => gameArea;

    [SerializeField]
    private AreaNormalised gameAreaNormalised;
    public AreaNormalised GameAreaNormalised => gameAreaNormalised;


    [SerializeField]
    private Area tileArea;
    public Area TileArea => tileArea;

    [SerializeField]
    private AreaNormalised tileAreaNormalised;
    public AreaNormalised TileAreaNormalised => tileAreaNormalised;



    public void Initialise(Camera camera, AreaNormalised gameAreaNorm, float tileAreaSideOffset)
    {
        screenArea = new Area(camera);
        screenAreaNormalised = new AreaNormalised(1f);

        gameAreaNormalised = gameAreaNorm;
        gameArea = screenArea * gameAreaNormalised;


        float tileLeftBound = (1f - tileAreaSideOffset) * gameArea.LeftBound;
        float tileRightBound = (1f - tileAreaSideOffset) * gameArea.RightBound;

        // Tile area is a square, so calculate only one dimension.
        float tileAreaSize = tileRightBound - tileLeftBound;

        tileArea = new Area(
            gameArea.TopBound,
            gameArea.TopBound - tileAreaSize,
            tileLeftBound,
            tileRightBound);

        tileAreaNormalised = tileArea / screenArea;
    }
}
