﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;



public class TileManager : MonoBehaviour
{
    [Serializable]
    public class TileSpawnInfo
    {
        public TileKind tileKind;
        public float probability;
    }

    [SerializeField]
    private Tile tilePrefab;
    [SerializeField]
    private TileSpawnInfo[] tileSpawnInfos;
    [SerializeField]
    [Range(0f, 1f)]
    private float tileSpawnProbability;
    [SerializeField]
    private float tileSpawnInterval;
    [SerializeField]
    private float tileMoveDuration;


    private Vector2[] spawnPoints;
    private TilePool tilePool = new TilePool();

    private float spawnTimer;


    public static TileManager Instance { get; private set; }



    private void Awake()
    {
        Instance = this;

        for (var i = 1; i < tileSpawnInfos.Length; i++)
        {
            tileSpawnInfos[i].probability += tileSpawnInfos[i - 1].probability;
        }

        TileMover.moveSpeed = TileMover.moveDistance / tileMoveDuration;

        spawnPoints = ScreenLayoutController.Instance.SpawnPoints;

        int gridSize = spawnPoints.Length;
        tilePool.Initialise(tilePrefab, gridSize * (gridSize + 1));

        spawnTimer = tileSpawnInterval;
    }


    public void ContinueSavedGame(SaveData saveData)
    {
        Debug.Log($"TileManager.ContinueSavedGame: loading {saveData.tileSaveDatas.Count} tiles.", this);

        spawnTimer = saveData.spawnTimer;

        foreach (var savedTile in saveData)
        {
            Debug.Log($"TileManager.ContinueSavedGame: spawning - {savedTile}");

            SpawnTile(savedTile);
        }
    }


    private void Update()
    {
        if (spawnTimer <= 0f)
        {
            spawnTimer = tileSpawnInterval;

            SpawnTileRow();
        }

        spawnTimer -= Time.deltaTime;
    }


    #region Spawning
    public void SpawnTileRow()
    {
        int spawned = 0;

        foreach (var spawnPoint in spawnPoints)
        {
            spawned += SpawnTileMaybe(spawnPoint) ? 1 : 0;
        }

        if (spawned == 0)
        {
            SpawnTileForced();
        }


        MoveTiles();
    }


    private bool SpawnTileMaybe(Vector2 spawnPoint)
    {
        if (RandomProvider.Double() < tileSpawnProbability)
        {
            SpawnTile(spawnPoint);

            return true;
        }

        return false;
    }


    private void SpawnTileForced()
    {
        int spawnPointIndex = RandomProvider.Int(0, spawnPoints.Length - 1);

        SpawnTile(spawnPoints[spawnPointIndex]);
    }


    private void SpawnTile(Vector2 spawnPoint)
    {
        tilePool.ActivateTile(CreateNewTile);

        void CreateNewTile(Tile tile)
        {
            tile.transform.position = spawnPoint;
            tile.SetTileKind(ChooseTileKind());
        }
    }

    private void SpawnTile(Tile.SaveData savedTile)
    {
        tilePool.ActivateTile(tile => tile.Activate(savedTile));
    }


    private TileKind ChooseTileKind()
    {
        var val = (float) RandomProvider.Double(0f, tileSpawnInfos.Last().probability);
        TileKind? chosenTile = Array.Find(tileSpawnInfos, info => val <= info.probability)?.tileKind;
        return chosenTile.GetValueOrDefault(tileSpawnInfos[0].tileKind);
    }
    #endregion


    private void MoveTiles()
    {
        foreach (Tile tile in tilePool)
        {
            tile.tileMover.StartMove();
        }

        StartCoroutine(StopTilesAfterDelay());

        IEnumerator StopTilesAfterDelay()
        {
            yield return new WaitForSeconds(tileMoveDuration);

            foreach (Tile tile in tilePool)
            {
                tile.tileMover.StopMove();
            }
        }
    }



    public void Deactivate(Tile tile)
    {
        tilePool.DeactivateTile(tile);
    }


    public SaveData GetSaveData()
    {
        var saveData = new SaveData();

        saveData.spawnTimer = spawnTimer;

        foreach (Tile tile in tilePool)
        {
            if (tile.GetSaveData(out Tile.SaveData tileData))
            {
                saveData.Add(tileData);
            }
        }

        Debug.Log($"TileManager.GetSaveData: saving {saveData.tileSaveDatas.Count} tiles.", this);

        return saveData;
    }


    [Serializable]
    public class SaveData : IEnumerable<Tile.SaveData>
    {
        public float spawnTimer;
        public List<Tile.SaveData> tileSaveDatas;


        public SaveData()
        {
            spawnTimer = 0f;
            tileSaveDatas = new List<Tile.SaveData>();
        }

        public SaveData(float spawnTimer, List<Tile.SaveData> tileSaveDatas)
        {
            this.spawnTimer = spawnTimer;
            this.tileSaveDatas = tileSaveDatas;
        }

        public void Add(Tile.SaveData saveData)
        {
            tileSaveDatas.Add(saveData);
        }

        public IEnumerator<Tile.SaveData> GetEnumerator()
        {
            return tileSaveDatas.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return tileSaveDatas.GetEnumerator();
        }
    }
}



public class TilePool : IEnumerable<Tile>
{
    private Queue<Tile> inactiveTiles = new Queue<Tile>();
    private List<Tile> activeTiles = new List<Tile>();


    public void Initialise(Tile tilePrefab, int amount)
    {
        for (var i = 0; i < amount; i++)
        {
            var tile = GameObject.Instantiate<Tile>(tilePrefab);
            tile.gameObject.name = "Tile " + i;
            tile.gameObject.SetActive(false);

            inactiveTiles.Enqueue(tile);
        }
    }


    public void DeactivateTile(Tile tile)
    {
        activeTiles.Remove(tile);
        inactiveTiles.Enqueue(tile);
    }


    public void ActivateTile(Action<Tile> initialiser)
    {
        Tile tile = inactiveTiles.Dequeue();

        initialiser(tile);

        activeTiles.Add(tile);
    }


    IEnumerator IEnumerable.GetEnumerator()
    {
        return ((IEnumerable<Tile>) activeTiles).GetEnumerator();
    }

    public IEnumerator<Tile> GetEnumerator()
    {
        return activeTiles.GetEnumerator();
    }
}
