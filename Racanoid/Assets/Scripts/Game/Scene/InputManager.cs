﻿using UnityEngine;
using UnityEngine.Events;



public class InputManager : MonoBehaviour
{
    [System.Serializable]
    public class TouchPositionEvent : UnityEvent<Vector2> { }

    public TouchPositionEvent touchStart;
    public TouchPositionEvent touchMove;
    public UnityEvent touchEnd;


    public static InputManager Instance { get; private set; }


    private Camera mainCamera;

    private bool hasInput = false;
    private int trackedTouchID;


    private void Awake()
    {
        touchStart = new TouchPositionEvent();
        touchMove = new TouchPositionEvent();
        touchEnd = new UnityEvent();

        mainCamera = Camera.main;

        hasInput = false;
        trackedTouchID = 0;

        Instance = this;
    }


    private void Start()
    {
        GameManager.Instance.resetState.AddListener(ResetInput);
    }


    private void Update()
    {
        HandleInput();

        if (Application.platform != RuntimePlatform.Android)
        {
            DebugInput();
        }
    }


    private void ResetInput()
    {
        hasInput = false;
        trackedTouchID = 0;
    }


    private void HandleInput()
    {
        if (Input.touchCount > 0)
        {
            Touch trackedTouch;

            if (hasInput)
            {
                Input.touches.TryFind(touch => touch.fingerId == trackedTouchID, out trackedTouch);
            }
            else
            {
                if (!Input.touches.TryFind(touch => touch.phase == TouchPhase.Began, out trackedTouch))
                {
                    touchEnd?.Invoke();

                    return;
                }
                else
                {
                    trackedTouchID = trackedTouch.fingerId;
                }
            }

            Vector2 touchPosition = ScreenToWorldPoint(trackedTouch.position);

            switch (trackedTouch.phase)
            {
                case TouchPhase.Began:
                {
                    if (IsInInputArea(touchPosition))
                    {
                        hasInput = true;
                        touchStart.Invoke(touchPosition);
                    }

                    break;
                }

                case TouchPhase.Moved:
                {
                    touchMove.Invoke(touchPosition);

                    break;
                }

                case TouchPhase.Canceled:
                case TouchPhase.Ended:
                {
                    ResetInput();

                    touchEnd.Invoke();

                    break;
                }
            }
        }
    }


    private Vector2 ScreenToWorldPoint(Vector2 screenPoint)
    {
        return mainCamera.ScreenToWorldPoint(screenPoint);
    }


    private bool IsInInputArea(Vector2 point)
    {
        return -3.5f < point.y && point.y < 0f;
    }



    #region Debug Input

    private void DebugInput()
    {
        Vector2 touchPosition = ScreenToWorldPoint(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            if (IsInInputArea(touchPosition))
            {
                touchStart.Invoke(touchPosition);
            }
        }
        else if (Input.GetMouseButton(0))
        {
            touchMove.Invoke(touchPosition);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            touchEnd.Invoke();
        }
    }

    #endregion
}
