﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;



public class GameUIController : MonoBehaviour
{
    [SerializeField]
    private TMP_Text scoreText;

    [SerializeField]
    private TMP_Text ballsText;

    [SerializeField]
    public Button menuButton;

    [SerializeField]
    private Image pauseTint;




    public void UpdateScoreText(int newScore)
    {
        scoreText.SetText(newScore.ToString());
    }


    public void UpdateBallsText(int newBalls)
    {
        ballsText.SetText(newBalls.ToString());
    }


    public void ShowPauseTint()
    {
        pauseTint.gameObject.SetActive(true);
    }

    public void HidePauseTint()
    {
        pauseTint.gameObject.SetActive(false);
    }
}
