﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;



public class GameManager : MonoBehaviour
{
    [SerializeField]
    private int startingBalls;
    [SerializeField]
    private GameUIController uiController;
    [SerializeField]
    private BallController ball;
    [SerializeField]
    private PlatformController platform;


    public event Action StoppingGame;

    public UnityEvent tileBreakTrough;
    public UnityEvent resetState;


    private int coins;
    private int score;
    public int Score
    {
        get => score;
        private set
        {
            score = value;
            uiController.UpdateScoreText(score);
        }
    }
    private int balls;
    public int Balls
    {
        get => balls;
        private set
        {
            balls = value;
            uiController.UpdateBallsText(balls);
        }
    }

    private bool hasAdContinue = false;


    public static GameManager Instance { get; private set; }



    private void Awake()
    {
        Instance = this;

        tileBreakTrough = new UnityEvent();
        resetState = new UnityEvent();

        uiController.menuButton.onClick.AddListener(GoToMenu);
    }


    private void Start()
    {
        InputManager.Instance.touchStart.AddListener(_ => Continue());
        InputManager.Instance.touchEnd.AddListener(Pause);

        GridBorder.Instance.tileBorderTouch.AddListener(OnTileBorderTouch);
        EdgeColliderManager.Instance.ballLost.AddListener(OnBallLost);

        InitData();

        Pause();
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            StoppingGame?.Invoke();

            GoToMenu();
        }
    }


    private void OnTileBorderTouch()
    {
        tileBreakTrough.Invoke();
        Pause();
        MaybeLoseGame();
    }


    private void Pause()
    {
        Time.timeScale = 0f;
        uiController.ShowPauseTint();
    }

    private void Continue()
    {
        uiController.HidePauseTint();
        Time.timeScale = 1f;
    }


    private void OnBallLost()
    {
        Pause();

        LoseBall();
    }


    private void InitData()
    {
        // Try to load saved data.
        if (!LoadGame())
        {
            // Reset if no saved data found.
            Score = 0;
            Balls = startingBalls;
        }
        // Load coins.
        coins = 0;
    }



    public void IncScore()
    {
        Score++;
    }


    public void IncCoins()
    {
        coins++;
    }


    public void IncBalls()
    {
        Balls++;
    }


    public void LoseBall()
    {
        if (--Balls <= 0)
        {
            MaybeLoseGame();
        }
        else
        {
            resetState.Invoke();
            uiController.UpdateBallsText(Balls);
        }
    }


    private void MaybeLoseGame()
    {
        // Offer player to watch an ad to continue game.
        if (hasAdContinue)
        {
            // Show ad continue menu.
        }
        else
        {
            GameOver();
        }
    }


    private void GameOver()
    {
        Storage.Delete("Game");

        // Save score and earned money data.

        // Show game result.

        LoadMenu();
    }


    private void GoToMenu()
    {
        SaveGame();

        LoadMenu();
    }


    private void LoadMenu()
    {
        CleanScene();

        SceneManager.LoadScene("Menu");
    }


    private void CleanScene()
    {
    }


    #region Save/Load

    [Serializable]
    public class SaveData
    {
        public int balls;
        public int score;

        public TileManager.SaveData tiles;
    }


    private void SaveGame()
    {

        var saveData = new SaveData()
        {
            balls = Balls,
            score = Score,
            tiles = TileManager.Instance.GetSaveData()
        };

        var formatter = new BinaryFormatter();
        var dataStream = new MemoryStream();

        formatter.Serialize(dataStream, saveData);

        Storage.Save("Game", dataStream);
    }


    private bool LoadGame()
    {
        if (Storage.Load("Game", out MemoryStream dataStream))
        {
            var formatter = new BinaryFormatter();
            var savedGame = (SaveData) formatter.Deserialize(dataStream);

            Balls = savedGame.balls;
            Score = savedGame.score;

            TileManager.Instance.ContinueSavedGame(savedGame.tiles);

            return true;
        }
        else
        {
            TileManager.Instance.SpawnTileRow();

            return false;
        }
    }

    #endregion Save/Load
}
