﻿using System;
using UnityEngine;



public class BallMover : MonoBehaviour
{
    [SerializeField]
    private float speed;
    [SerializeField]
    private float maxAffectedAngle;
    [SerializeField]
    private float affectAmount;


    private new Rigidbody2D rigidbody;
    private Area ballArea;


    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }


    private void Start()
    {
        var ballRadius = GetComponent<SpriteRenderer>().bounds.extents.x;

        ballArea = ScreenLayoutController.Instance.ScreenLayout.GameArea;
        ballArea = new Area(
            ballArea.TopBound - ballRadius,
            ballArea.BottomBound + ballRadius,
            ballArea.LeftBound + ballRadius,
            ballArea.RightBound - ballRadius);

        ResetVelocity();
    }


    private void FixedUpdate()
    {
        Vector2 velocityNorm = rigidbody.velocity.normalized;

        if (Mathf.Abs(velocityNorm.y) <= maxAffectedAngle)
        {
            velocityNorm.y += Mathf.Sign(velocityNorm.y) * affectAmount;
        }
        else if (Mathf.Abs(velocityNorm.x) <= maxAffectedAngle)
        {
            velocityNorm.x += Mathf.Sign(velocityNorm.y) * affectAmount;
        }

        rigidbody.velocity = velocityNorm.normalized * speed;

        CheckEdgeColliderBreakThrough();
    }


    public void ResetVelocity()
    {
        rigidbody.velocity = Vector2.one * speed;
    }


    private void CheckEdgeColliderBreakThrough()
    {
        float newX = Mathf.Max(ballArea.LeftBound, rigidbody.position.x);
        newX = Mathf.Min(newX, ballArea.RightBound);

        float newY = Mathf.Max(ballArea.BottomBound, rigidbody.position.y);
        newY = Mathf.Min(newY, ballArea.TopBound);

        rigidbody.position = new Vector2(newX, newY);
    }
}
