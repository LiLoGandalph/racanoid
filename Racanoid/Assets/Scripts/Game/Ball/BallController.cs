﻿using UnityEngine;



[RequireComponent(typeof(Collider2D))]
public class BallController : MonoBehaviour
{
    [SerializeField]
    private PlatformController platform;


    private BallMover ballMover;
    private float startPosY;


    private void Awake()
    {
        ballMover = GetComponent<BallMover>();

        AdjustScale();

        float diameter = GetComponent<SpriteRenderer>().bounds.size.y;
        startPosY = platform.PositionY + 2 * diameter;
    }


    private void Start()
    {
        GameManager.Instance.resetState.AddListener(ResetBall);

        ResetBall();
    }


    public void ResetBall()
    {
        transform.position = new Vector2(0f, startPosY);

        ballMover.ResetVelocity();
    }


    private void AdjustScale()
    {
        float updScale = ScreenLayoutController.Instance.UpdatedTileScale;
        transform.localScale = new Vector2(updScale, updScale);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        var tile = collision.gameObject.GetComponent<HealthSystem>();

        if (tile == null)
            return;

        tile.TakeDamage();
    }
}
