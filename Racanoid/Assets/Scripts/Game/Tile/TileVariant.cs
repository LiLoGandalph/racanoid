﻿using UnityEngine;



[CreateAssetMenu(menuName = "New Racanoid Tile", fileName = "Tile")]
public class TileVariant : ScriptableObject
{
    [Header("Kind")]
    public TileKind kind;

    [Header("Health")]
    public bool shouldShowHealth;
    public bool shouldComputeInitialHealth;
    public int defaultInitialHealth;

    [Header("Stats Increase")]
    public bool shouldIncreaseScore;
    public bool shouldIncreaseCoins;
    public bool shouldIncreaseBalls;
    public bool shouldIncreaseDamage;

    [Header("Explosion")]
    public bool shouldDestroyTilesAround;
    public float destroyRadius;
}