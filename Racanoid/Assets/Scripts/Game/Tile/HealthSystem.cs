﻿using System;
using TMPro;
using UnityEngine;



public class HealthSystem : MonoBehaviour
{
    #region Editor-set data
    [SerializeField]
    private TMP_Text healthText;

    [Header("Health Initialisation")]
    [SerializeField]
    private float initialHealthMultiplier = 1f;
    [SerializeField]
    private float randomHealthMultiplierMin = 0.8f;
    [SerializeField]
    private float randomHealthMultiplierMax = 1.2f;

    [Header("Animation")]
    [SerializeField]
    private Animation deathAnimation;
    #endregion

    private int health;
    private int Health
    {
        get => health;
        set
        {
            health = Mathf.Max(0, value);
            if (shouldShowHealth)
            {
                UpdateHealthText();
            }
        }
    }
    private bool shouldShowHealth;
    public bool IsAlive { get; private set; }


    public delegate void TileDeathHandler(float deathAnimationLength);
    public event TileDeathHandler Death;


    public void Reset(bool shouldShowHealth, bool shouldComputeHealth, int defaultHealth)
    {
        this.shouldShowHealth = shouldShowHealth;

        Health = shouldComputeHealth ? ComputeHealth() : defaultHealth;
        IsAlive = true;
    }


    public void Reset(SaveData saveData)
    {
        this.shouldShowHealth = saveData.shouldShowHealth;
        Health = saveData.health;

        IsAlive = true;
    }


    private int ComputeHealth()
    {
        float rawHealth = GameManager.Instance.Score * initialHealthMultiplier * UnityEngine.Random.Range(randomHealthMultiplierMin, randomHealthMultiplierMax);
        return Mathf.Max(1, Mathf.CeilToInt(rawHealth));
    }


    public void TakeDamage()
    {
        if (IsAlive)
        {

            Health -= GameManager.Instance.Balls;
            if (Health <= 0)
            {
                Die();
            }
        }
    }


    private void UpdateHealthText()
    {
        healthText.SetText(Health.ToString());
    }


    public void Die()
    {
        if (IsAlive)
        {
            IsAlive = false;
            deathAnimation.Play();
            Death(deathAnimation.clip.length);
        }
    }


    public bool GetSaveData(out SaveData saveData)
    {
        saveData = new SaveData(Health, shouldShowHealth);
        return IsAlive;
    }


    [Serializable]
    public class SaveData
    {
        public int health;
        public bool shouldShowHealth;

        public SaveData(int health, bool shouldShowHealth)
        {
            this.health = health;
            this.shouldShowHealth = shouldShowHealth;
        }


        public override string ToString()
        {
            return $"Health.SaveData {health} {shouldShowHealth}";
        }
    }
}