﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;



public class Tile : MonoBehaviour
{
    [Serializable]
    private class TileKindRenderer
    {
        public TileKind kind;
        public Renderer renderer;
    }


    [SerializeField]
    private TileVariant[] tileVariants;
    [SerializeField]
    private TileKindRenderer[] renderers;

    [Header("Tile Scripts")]
    [SerializeField]
    private HealthSystem healthSystem;
    public TileMover tileMover;


    private TileVariant currentVariant;
    private TMP_Text healthText;


    private void Awake()
    {
        healthSystem.Death += OnDeath;

        this.gameObject.SetActive(false);
    }

    public void Activate(TileKind newKind)
    {
        SetTileKind(newKind);
    }

    public void Activate(SaveData saveData)
    {
        currentVariant = Array.Find(tileVariants, variant => variant.kind == saveData.kind);

        healthSystem.Reset(saveData.healthData);
        tileMover.Reset(saveData.moverData);

        ChangeRenderersState();

        float scale = ScreenLayoutController.Instance.UpdatedTileScale;
        transform.localScale = new Vector2(scale, scale);

        this.gameObject.SetActive(true);
    }


    #region Tile kind swapping
    public void SetTileKind(TileKind newKind)
    {
        currentVariant = Array.Find(tileVariants, variant => variant.kind == newKind);

        bool shouldShowHealth = currentVariant.shouldShowHealth;
        healthSystem.Reset(shouldShowHealth, currentVariant.shouldComputeInitialHealth, currentVariant.defaultInitialHealth);

        ChangeRenderersState();

        float scale = ScreenLayoutController.Instance.UpdatedTileScale;
        transform.localScale = new Vector2(scale, scale);

        this.gameObject.SetActive(true);
    }


    private void ChangeRenderersState()
    {
        foreach (var rend in renderers)
        {
            if (rend.kind == currentVariant.kind)
            {
                rend.renderer.enabled = true;
            }
            else
            {
                rend.renderer.enabled = false;
            }
        }
    }
    #endregion


    private void OnDeath(float deathAnimationLength)
    {
        ExecuteDestroyEffects();

        StartCoroutine(AfterDeathCleaning(deathAnimationLength));
    }


    private IEnumerator AfterDeathCleaning(float timeToWait)
    {
        yield return new WaitForSeconds(timeToWait);

        TileManager.Instance.Deactivate(this);
        gameObject.SetActive(false);
    }


    private void ExecuteDestroyEffects()
    {
        if (currentVariant.shouldIncreaseScore)
        {
            GameManager.Instance.IncScore();
        }

        if (currentVariant.shouldIncreaseBalls)
        {
            GameManager.Instance.IncBalls();
        }

        if (currentVariant.shouldIncreaseCoins)
        {
            GameManager.Instance.IncCoins();
        }

        if (currentVariant.shouldDestroyTilesAround)
        {
            DestroyTilesAround(currentVariant.destroyRadius);
        }
    }


    private void DestroyTilesAround(float radius)
    {
        RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, radius, Vector2.zero);

        foreach (var hit in hits)
        {
            var tileHealth = hit.transform.gameObject.GetComponentInParent<HealthSystem>();
            if (tileHealth != null)
            {
                tileHealth.Die();
            }
        }
    }


    public bool GetSaveData(out SaveData saveData)
    {
        if (healthSystem.GetSaveData(out HealthSystem.SaveData healthData))
        {
            TileMover.SaveData moverData = tileMover.GetSaveData();

            saveData = new SaveData(currentVariant.kind, healthData, moverData);

            Debug.Log($"{gameObject.name}: {saveData}");

            return true;
        }
        else
        {
            Debug.Log($"{gameObject.name}: I'm dying((");

            saveData = null;
            return false;
        }
    }


    [Serializable]
    public class SaveData
    {
        public TileKind kind;

        public HealthSystem.SaveData healthData;
        public TileMover.SaveData moverData;


        public SaveData(TileKind kind, HealthSystem.SaveData healthData, TileMover.SaveData moverData)
        {
            this.kind = kind;

            this.healthData = healthData;
            this.moverData = moverData;
        }


        public override string ToString()
        {
            return $"Tile.SaveData {kind} ({healthData}) ({moverData})";
        }
    }
}