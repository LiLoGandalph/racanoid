﻿using SerializableTypes;
using System;
using System.Collections;
using System.Runtime.Serialization;
using UnityEngine;



public class TileMover : MonoBehaviour
{
    public static float moveDistance;
    public static float moveSpeed;

    private new Rigidbody2D rigidbody;
    private Vector2 nextMovePosition = new Vector2();
    private bool isMoving;


    public void Awake()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        isMoving = false;
    }


    public void StartMove()
    {
        if (this.gameObject.activeInHierarchy)
        {
            isMoving = true;

            nextMovePosition = transform.position;
            nextMovePosition.y -= moveDistance;

            StartCoroutine(Move());
        }
    }


    private IEnumerator Move()
    {
        do
        {
            var positionDelta = (nextMovePosition - rigidbody.position).normalized;
            positionDelta *= moveSpeed * Time.fixedDeltaTime;

            rigidbody.MovePosition(rigidbody.position + positionDelta);

            yield return new WaitForFixedUpdate();
        }
        while (isMoving);
    }


    internal void StopMove()
    {
        isMoving = false;
    }



    private void OnDestroy()
    {
        StopAllCoroutines();
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }


    public SaveData GetSaveData()
    {
        return new SaveData(isMoving ? nextMovePosition : (Vector2) transform.position);
    }


    public void Reset(SaveData saveData)
    {
        transform.position = saveData.position;
    }


    [Serializable]
    public class SaveData : ISerializable
    {
        public Vector2 position;

        public SaveData(Vector2 position)
        {
            this.position = position;
        }

        public override string ToString()
        {
            return $"Mover.SaveData {position}";
        }

        #region Serialization
        private const string Position = "position";

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue(Position, new Vector2_Ser(position));
        }

        public SaveData(SerializationInfo info, StreamingContext context)
        {
            this.position = (Vector2_Ser) info.GetValue(Position, typeof(Vector2_Ser));
        }
        #endregion
    }
}
