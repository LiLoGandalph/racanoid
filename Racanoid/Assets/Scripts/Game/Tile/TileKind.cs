﻿public enum TileKind
{
    Healthy,
    Coin,
    Ball,
    Bomb
}
