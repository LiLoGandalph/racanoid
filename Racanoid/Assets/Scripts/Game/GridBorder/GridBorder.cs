﻿using UnityEngine;
using UnityEngine.Events;



[RequireComponent(typeof(SpriteRenderer), typeof(Collider2D))]
public class GridBorder : MonoBehaviour
{
    public UnityEvent tileBorderTouch;

    public static GridBorder Instance { get; private set; }

    private static bool isTouched;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!isTouched)
        {
            var tileHealth = collision.gameObject.GetComponent<HealthSystem>();
            if (tileHealth != null && tileHealth.IsAlive)
            {
                isTouched = true;
                tileBorderTouch.Invoke();
            }
        }
    }


    private void Awake()
    {
        Instance = this;

        tileBorderTouch = new UnityEvent();

        isTouched = false;

        UpdateScale();
        UpdatePosition();
    }


    private void UpdatePosition()
    {
        float tileBottom = ScreenLayoutController.Instance.ScreenLayout.TileArea.BottomBound;
        float borderHeight = GetComponent<SpriteRenderer>().bounds.size.y;
        transform.position = new Vector2(0f, tileBottom - borderHeight);
    }


    private void UpdateScale()
    {
        transform.localScale = Vector2.one * ScreenLayoutController.Instance.UpdatedTileScale;
    }
}
