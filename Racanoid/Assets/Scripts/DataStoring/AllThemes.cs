﻿using UnityEngine;

[System.Serializable]
public class AllThemes
{
    public UnlockableTheme[] Themes { get; private set; }


    public AllThemes()
    {
        Themes = new UnlockableTheme[]
        {
            new UnlockableTheme(new Theme(new Color(80f  / 255f, 100f / 255f, 240f / 255f)), true ),
            new UnlockableTheme(new Theme(new Color(148f / 255f, 224f / 255f, 80f  / 255f)), false),
            new UnlockableTheme(new Theme(new Color(240f / 255f, 97f  / 255f, 45f  / 255f)), false),
            new UnlockableTheme(new Theme(new Color(220f / 255f, 234f / 255f, 71f  / 255f)), false),
            new UnlockableTheme(new Theme(new Color(55f  / 255f, 16f  / 255f, 222f / 255f)), false),
            new UnlockableTheme(new Theme(new Color(169f / 255f, 26f  / 255f, 8f   / 255f)), false),
            new UnlockableTheme(new Theme(new Color(220f / 255f, 98f  / 255f, 197f / 255f)), false),
            new UnlockableTheme(new Theme(new Color(159f / 255f, 21f  / 255f, 204f / 255f)), false),
            new UnlockableTheme(new Theme(new Color(5f   / 255f, 46f  / 255f, 212f / 255f)), false),
            new UnlockableTheme(new Theme(new Color(230f / 255f, 24f  / 255f, 42f  / 255f)), false)
        };
    }
}
