﻿using System.IO;
using UnityEngine;



public static class Storage
{
    public static void Save(string key, MemoryStream dataStream)
    {
        Debug.Log($"Storage.Save: saving data to {GetFilePath(key)}");

        File.WriteAllBytes(GetFilePath(key), dataStream.ToArray());
    }


    public static bool Load(string key, out MemoryStream dataStream)
    {
        string filePath = GetFilePath(key);

        Debug.Log($"Storage.Load: trying to load data from {filePath} (it {(File.Exists(filePath) ? "exists" : "doesn't exist")})");

        if (File.Exists(filePath))
        {
            byte[] bytes = File.ReadAllBytes(filePath);
            dataStream = new MemoryStream(bytes);
            return true;
        }
        else
        {
            dataStream = null;
            return false;
        }
    }


    private static string GetFilePath(string key)
    {
        return Application.persistentDataPath + "/" + key + ".lulkek";
    }

    public static void Delete(string key)
    {
        File.Delete(GetFilePath(key));
    }
}