﻿using System;
using UnityEngine;



namespace SerializableTypes
{
    [Serializable]
    public struct Color_Ser
    {
        public float r;
        public float g;
        public float b;
        public float a;


        public Color_Ser(Color color)
        {
            this.r = color.r;
            this.g = color.g;
            this.b = color.b;
            this.a = color.a;
        }


        public Color ToColor()
        {
            return new Color(r, g, b, a);
        }


        #region Type conversions

        public static implicit operator Color_Ser(Color color)
        {
            return new Color_Ser(color);
        }

        public static implicit operator Color(Color_Ser serColor)
        {
            return serColor.ToColor();
        }

        #endregion
    }


    [Serializable]
    public struct Vector3_Ser
    {
        public float x;
        public float y;
        public float z;


        public Vector3_Ser(Vector3 vector)
        {
            this.x = vector.x;
            this.y = vector.y;
            this.z = vector.z;
        }


        public Vector3 ToVector3()
        {
            return new Vector3(x, y, z);
        }


        #region Type conversions

        public static implicit operator Vector3_Ser(Vector3 vector)
        {
            return new Vector3_Ser(vector);
        }

        public static implicit operator Vector3(Vector3_Ser serVector)
        {
            return serVector.ToVector3();
        }

        #endregion
    }


    [Serializable]
    public struct Vector2_Ser
    {
        public float x;
        public float y;


        public Vector2_Ser(Vector2 vector2)
        {
            this.x = vector2.x;
            this.y = vector2.y;
        }


        public Vector2 ToVector2()
        {
            return new Vector2(x, y);
        }


        #region Type conversions

        public static implicit operator Vector2_Ser(Vector2 vector)
        {
            return new Vector2_Ser(vector);
        }

        public static implicit operator Vector2(Vector2_Ser serVector)
        {
            return serVector.ToVector2();
        }

        #endregion
    }


    [Serializable]
    public struct Quaternion_Ser
    {
        public float x;
        public float y;
        public float z;
        public float w;


        public Quaternion_Ser(Quaternion quaternion)
        {
            this.x = quaternion.x;
            this.y = quaternion.y;
            this.z = quaternion.z;
            this.w = quaternion.w;
        }

        public Quaternion ToQuaternion()
        {
            return new Quaternion(x, y, z, w);
        }


        #region Type conversions

        public static implicit operator Quaternion_Ser(Quaternion quaternion)
        {
            return new Quaternion_Ser(quaternion);
        }

        public static implicit operator Quaternion(Quaternion_Ser serQuaternion)
        {
            return serQuaternion.ToQuaternion();
        }

        #endregion
    }
}
