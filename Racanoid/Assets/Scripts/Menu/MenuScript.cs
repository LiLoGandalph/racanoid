﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class MenuScript : MonoBehaviour
{
    private const string isSoundOn = "isSoundOn";
    private const string isMusicOn = "isMusicOn";

    [Header("Buttons")]
    [SerializeField]
    private Button playButton;
    [SerializeField]
    private Button customisationMenuButton;
    [SerializeField]
    private Button shareButton;

    [Header("Toggles")]
    [SerializeField]
    private Toggle soundToggle;
    [SerializeField]
    private Toggle musicToggle;


    private void Awake()
    {
        playButton.onClick.AddListener(LoadGame);
        customisationMenuButton.onClick.AddListener(LoadCustomisationMenu);
        shareButton.onClick.AddListener(Share);

        soundToggle.onValueChanged.AddListener(TurnSoundOn);
        musicToggle.onValueChanged.AddListener(TurnMusicOn);

        LoadSettings();
    }


    private void LoadGame()
    {
        SaveSettings();

        SceneManager.LoadScene("Game");
    }


    private void LoadCustomisationMenu()
    {
        SaveSettings();

        SceneManager.LoadScene("CustomisationMenu");
    }


    private void SaveSettings()
    {
        PlayerPrefs.SetInt(isSoundOn, BoolToInt(soundToggle.isOn));
        PlayerPrefs.SetInt(isMusicOn, BoolToInt(musicToggle.isOn));

        int BoolToInt(bool b) => b ? 1 : 0;
    }

    private void LoadSettings()
    {
        soundToggle.isOn = PlayerPrefs.HasKey(isSoundOn) ? GetIntAsBool(isSoundOn) : true;
        musicToggle.isOn = PlayerPrefs.HasKey(isMusicOn) ? GetIntAsBool(isMusicOn) : true;

        bool GetIntAsBool(string key) => PlayerPrefs.GetInt(key) == 1;
    }


    private void TurnSoundOn(bool isOn)
    {
        // Turn on/off sound effects via audio mixer.
    }


    private void TurnMusicOn(bool isOn)
    {
        // Turn on/off music via audio mixer.
    }


    private void Share()
    {
        // Share using Android native stuff.
    }
}